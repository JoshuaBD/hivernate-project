package dao;

import java.util.List;

import projecte.Client;

public interface IDaoClient extends IGenericDao<Client,Integer>{

	void saveOrUpdate(Client a);

	Client get(Integer id);

	List<Client> list();

	void delete(Integer id);
}
