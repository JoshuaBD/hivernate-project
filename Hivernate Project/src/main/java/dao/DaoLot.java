package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import projecte.Lot;
import projecte.Producte;

public class DaoLot extends GenericDao<Lot, Integer> implements IDaoLot {

	@Override
	public void saveOrUpdate(Lot l) {
		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();
			Producte p = l.getProducte();

			Object obj =  session.get(getEntityClass(), l.getIdLot());
			
			if (null!=obj) { // Si ya existe, le quitamos la cantidad antes de modificar
				Lot lotAntic = (Lot) obj;
				lotAntic.getProducte().setStock(lotAntic.getProducte().getStock() - lotAntic.getQuantitat());
				lotAntic.setQuantitat(l.getQuantitat());
				lotAntic.getProducte().setStock(lotAntic.getProducte().getStock() + lotAntic.getQuantitat());
				session.saveOrUpdate(lotAntic);
				session.saveOrUpdate(lotAntic.getProducte());
				session.getTransaction().commit();
			} else {

				p.setStock(p.getStock() + l.getQuantitat());
				session.saveOrUpdate(l);
				session.saveOrUpdate(p);
				session.getTransaction().commit();
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
	}

	@Override
	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Lot lot = (Lot) session.get(getEntityClass(), id);
			Producte p = lot.getProducte();
			p.setStock(p.getStock() - lot.getQuantitat());
			session.delete(lot);
			session.saveOrUpdate(p);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
	}
}
