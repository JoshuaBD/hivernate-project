package dao;

import java.util.List;

import projecte.PeticioProveidor;
import projecte.Producte;

public interface IDaoProducte extends IGenericDao<Producte,Integer>{

	void saveOrUpdate(Producte p);

	Producte get(Integer id);

	List<Producte> list();

	void delete(Integer id);
	
	boolean afegirComponent(Producte p1, Producte p2);
	
	public boolean restarStock(Producte p, int quant);

	boolean afegirPeticio(PeticioProveidor p, Producte prod, boolean trans);
}
