package projecte;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "Proveidors")
public class Proveidor implements Serializable{

	@Id
	@Column(name = "id_proveidor")
	protected int idProveidor;

	@Column(name = "nom_proveidor")
	protected String nomProveidor;

	@Column(name = "cif")
	protected String CIF;
	
	@Type(type="numeric_boolean")
	@Column(name = "actiu")
	protected boolean actiu;

	@Column(name = "tipus")
	protected String tipus;

	@Column(name = "persona_contacte")
	protected String personaContacte;

	// relacio 1 a n amb peticioproveidor
	@OneToMany(mappedBy = "proveidor")
	Set<PeticioProveidor> peticions = new HashSet<PeticioProveidor>();

	// relacio 1 a 1 amb address
	@JoinColumn(name = "address", nullable = false)
	@OneToOne(cascade = CascadeType.PERSIST)
	private Address address;
	
	public Proveidor() {
		super();
	}

	public Proveidor(String nomProveidor, String cIF, boolean actiu, String tipus,
			String personaContacte, Address address) {
		super();
		this.nomProveidor = nomProveidor;
		CIF = cIF;
		this.actiu = actiu;
		this.tipus = tipus;
		this.personaContacte = personaContacte;
		this.address = address;
	}

	public int getIdProveidor() {
		return idProveidor;
	}

	public void setIdProveidor(int idProveidor) {
		this.idProveidor = idProveidor;
	}

	public String getNomProveidor() {
		return nomProveidor;
	}

	public void setNomProveidor(String nomProveidor) {
		this.nomProveidor = nomProveidor;
	}

	public String getCIF() {
		return CIF;
	}

	public void setCIF(String cIF) {
		CIF = cIF;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	public String getTipus() {
		return tipus;
	}

	public void setTipus(String tipus) {
		this.tipus = tipus;
	}

	public String getPersonaContacte() {
		return personaContacte;
	}

	public void setPersonaContacte(String personaContacte) {
		this.personaContacte = personaContacte;
	}

	public Set<PeticioProveidor> getPeticions() {
		return peticions;
	}

	public void setPeticions(Set<PeticioProveidor> Peticions) {
		this.peticions = Peticions;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 4;
		result = prime * result + idProveidor;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Proveidor))
			return false;
		Proveidor other = (Proveidor) obj;
		if (idProveidor != other.idProveidor)
			return false;
		return true;
	}
}